from AwesomeHub import Param, Device, Host


class Fan(Device):
    params = [
        Param({
            "name": "_power",
            "type": "Boolean",
            "value": True,
            "restrictions": {}
        }),
        Param({
            "name": "_speed",
            "type": "Number",
            "value": 13,
            "restrictions": {
                "min": 0,
                "max": 255
            }
        })
    ]

    def update(self, param):
        print "update", param

    def interval(self):
        print "interval", self.params[0].name


devices = [
    Fan('3', 'Fan 1')
]

if __name__ == '__main__':
    Host.run(devices=devices, port=3003)
