from AwesomeHub import Param, Device, Host


class Light(Device):
    params = [
        Param({
            "name": "_power",
            "type": "Boolean",
            "value": True,
            "restrictions": {}
        }),
        Param({
            "name": "_dimmer",
            "type": "Number",
            "value": 123,
            "restrictions": {
                "min": 0,
                "max": 255
            }
        })
    ]

    def initialize(self):
        print "initializing", self.id

    def update(self, param):
        print "update", param

    def interval(self):
        print "interval", self.params[0].name


devices = [
    Light('1', 'Light 1'),
    Light('2', 'Light 2')
]

if __name__ == '__main__':
    Host.run(devices=devices, port=3002)
