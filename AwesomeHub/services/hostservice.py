import requests


class HostService():
    hosts = []
    devices = {}

    def __init__(self):
        pass

    @staticmethod
    def get_hosts():
        return HostService.hosts

    @staticmethod
    def get_device_host(device_id):
        try:
            return HostService.devices[device_id]
        except Exception as e:
            return None

    @staticmethod
    def set_hosts(hosts):
        HostService.hosts = hosts
        for host in hosts:
            devices = HostService.fetch_host_devices(host)
            for device in devices:
                HostService.devices[device['id']] = host

    @staticmethod
    def fetch_host_devices(host):
        try:
            response = requests.get(host + '/devices')
            return response.json()
        except Exception as e:
            print host, e
            return []
