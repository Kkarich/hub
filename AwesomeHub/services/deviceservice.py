class DeviceService():
    def __init__(self):
        pass

    @staticmethod
    def get_devices():
        return DeviceService.devices

    @staticmethod
    def set_devices(devices):
        DeviceService.devices = devices

    @staticmethod
    def add_device(device):
        DeviceService.devices.append(device)

    @staticmethod
    def find_device(device_id):
        for device in DeviceService.devices:
            if device.id == device_id:
                return device
