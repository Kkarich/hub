from flask import Flask
from flask_restful import Api
from resources.devices import DeviceList, Devices
from resources.params import Params
from services.deviceservice import DeviceService

app = Flask(__name__)
api = Api(app)

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


api.add_resource(DeviceList, '/devices')
api.add_resource(Devices, '/devices/<device_id>')
api.add_resource(Params, '/devices/<device_id>/params/<param_name>')


def run(devices=[], port=3001):
    DeviceService.set_devices(devices)
    app.run(debug=True, host='0.0.0.0', port=port)