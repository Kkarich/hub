import threading


def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()

    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t


class Device(object):
    def __init__(self, id, name):
        self.id = id
        self.name = name

        # set_interval(self.interval, 60)
        self.initialize()

    def get(self, param_name):
        for param in self.params:
            if param.name == param_name:
                return param
        return None

    def set(self, param_name, value):
        param = self.get(param_name)

        if param is None:
            raise 'Param {} does not exist on device'.format(param_name)
        param.set_value(value)
        self.update(param_name)
        return param

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "params": [param.to_dict() for param in self.params]
        }

    def initialize(self):
        pass

    def update(self, param):
        pass

    def interval(self):
        pass
