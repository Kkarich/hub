class Param(object):
    def __init__(self, data):
        self.name = data.get('name')
        self.type = data.get('type')
        self.restrictions = data.get('restrictions')
        self.value = data.get('value')

    def set_value(self, value):
        self.value = value

    def get_value(self):
        return self.value

    def to_dict(self):
        return {
            "name": self.name,
            "type": self.type,
            "value": self.get_value(),
            "restrictions": self.restrictions
        }