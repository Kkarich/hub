from flask import Flask
from flask_restful import Api
from resources.devices import HubDeviceList, HubDevices
from resources.params import HubParams
from services.hostservice import HostService

app = Flask(__name__)
api = Api(app)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


api.add_resource(HubDeviceList, '/devices')
api.add_resource(HubDevices, '/devices/<device_id>')
api.add_resource(HubParams, '/devices/<device_id>/params/<param_name>')


def run(hosts=[], port=3001):
    print hosts
    HostService.set_hosts(hosts)
    app.run(debug=True, host='0.0.0.0', port=port)
