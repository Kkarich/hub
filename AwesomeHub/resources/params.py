import requests
import json
from flask_restful import Resource, abort, request
from ..services.deviceservice import DeviceService
from ..services.hostservice import HostService


class Params(Resource):
    def get(self, device_id, param_name):
        device = DeviceService.find_device(device_id)
        param = device.get(param_name)

        if param is None:
            abort(requests.codes.not_found,
                  message="There is no param with name {} on device {}".format(param_name, device.name))

        return param.to_dict()

    def post(self, device_id, param_name):
        device = DeviceService.find_device(device_id)
        try:
            data = json.loads(request.data)
            param = device.set(param_name, data['value'])
        except Exception as e:
            print e
            abort(requests.codes.not_found,
                  message="Could not update param {} for device {}, failed with error: {}".format(param_name,
                                                                                                  device.name, e))
        return param.to_dict()


class HubParams(Resource):
    def get(self, device_id, param_name):
        host = HostService.get_device_host(device_id)
        response = requests.get(host + '/devices/' + device_id + '/params/' + param_name)
        return response.json()

    def post(self, device_id, param_name):
        host = HostService.get_device_host(device_id)
        response = requests.post(host + '/devices/' + device_id + '/params/' + param_name, data=request.data)
        return response.json()
