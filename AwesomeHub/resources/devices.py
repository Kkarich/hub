import requests
from flask_restful import Resource, abort
from ..services.hostservice import HostService
from ..services.deviceservice import DeviceService


class Devices(Resource):
    def get(self, device_id):
        device = DeviceService.find_device(device_id)
        if device is None:
            abort(requests.codes.not_found, message="There is no device with id {}".format(device_id))
        return device.to_dict()


class DeviceList(Resource):
    def get(self):
        return [device.to_dict() for device in DeviceService.get_devices()]


class HubDevices(Resource):
    def get(self, device_id):
        host = HostService.get_device_host(device_id)
        if host is None:
            abort(requests.codes.not_found, message="Could not find host for device id {}".format(device_id))
        try:
            response = requests.get(host + '/devices/' + device_id)
            return response.json()
        except Exception as e:
            return {}


class HubDeviceList(Resource):
    def get(self):
        hosts = HostService.get_hosts()
        devices = []
        for host in hosts:
            try:
                response = requests.get(host + '/devices')
                devices.extend(response.json())
            except Exception as e:
                print 'Error occurred', e
        return devices
