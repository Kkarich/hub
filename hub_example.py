from AwesomeHub import Hub

hosts = [
    'http://localhost:3002',
    'http://localhost:3003'
]

if __name__ == '__main__':
    Hub.run(hosts=hosts, port=3001)
