This project contains 2 main applications right now.
1. The Host application
1. The ui application

Ideally these should be split into separate repos but it is easiest to keep them together for now.

# Host application

This is a python flask web server, stored in the `host` folder. This should be the application that is run on a raspberry pi that params connected devices.
It can also be run on the same machine as a `ui` application in a separate port. (this is done locally)

To run this application locally, open a terminal at the `./host` directory and run `python app.py`

# UI application

The ui application is a React application that communicates with all the Hosts. This should be able to run on the mirror,
the car dashboard, a phone, laptop, TV, ... or any other UI.

To run this application locally, open a terminal at the `./ui` directory and run the following commands

1. `npm install` - this installs all the dependencies
1. `yarn start` or `npm start` - this runs the ui application
