import React from 'react';
import MockData from './mock_data';
import Screen from './components/Screen';

class App extends React.Component {
    render() {
        return (<div className="App">
            <Screen hub={MockData.hub} components={MockData.components}/>
        </div>)
    }
}

export default App;