// This is an example of what would be stored in the DB
export default {
    "hub": "http://localhost:3001",
    // List of all components the user has set up
    "components": [
        {
            "name": "Basement Lights 1 Power",
            "type": "Device",
            "deviceId": "1",
            "paramName": "_power"
        },
        {
            "name": "Basement Lights 1 Dimmer",
            "type": "Device",
            "deviceId": "1",
            "paramName": "_dimmer"
        },
        {
            "name": "Basement Lights 2 Power",
            "type": "Device",
            "deviceId": "2",
            "paramName": "_power"
        }
    ]
}