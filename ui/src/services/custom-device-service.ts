// const MOCK_ENDPOINT: any = {
//     "10.10.10.11": {
//         "1": {
//             "id": "1",
//             "name": "Fan",
//             "host": "10.10.10.10",
//             "_power": {
//                 "name": "_power",
//                 "value": false,
//                 "type": "Boolean",
//                 "restrictions": {}
//             },
//             "_speed": {
//                 "name": "_speed",
//                 "value": 20,
//                 "type": "Number",
//                 "restrictions": {
//                     "min": 0,
//                     "max": 255
//                 }
//             }
//         }
//     }
// };

class CustomDeviceService {
    cache: any;

    constructor() {
        this.cache = {};
    }

    async getConfig(host: string, deviceId?: string, paramName?: string) {
        if (!this.cache[host]) {
            const result = await this.fetchConfig(host);
            if (!result) {
                throw `Could not find host: ${host}`;
            }
            this.cache[host] = result;
        }

        if (deviceId) {
            const device = this.cache[host].find((device: any) => {
                return device.id === deviceId;
            });
            if (!device) {
                throw `Could not find param: ${deviceId} at host ${host}`;
            }

            if (paramName) {
                const param = device.params.find((param: any) => {
                    return param.name === paramName;
                });
                if (!param) {
                    throw `Could not find param: ${paramName} at host ${host} for param ${deviceId}`;
                }

                return param;
            }
            return device;
        }

        return this.cache[host];
    }

    async fetchConfig(host: string) {
        const response = await fetch(`${host}/devices`);
        return response.json();
    }

    async updateControl(host: string, deviceId: string, paramName: string, value: number, restrictions?: any) {
        const response = await fetch(`${host}/devices/${deviceId}/params/${paramName}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'post',
            body: JSON.stringify({value})
        });

        // TODO: Update cache here
        const param = response.json();
        return param;
    }
}

export default new CustomDeviceService();