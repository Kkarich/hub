import React from 'react';
import CustomDeviceService from '../services/custom-device-service';
import {Boolean, Number, Stream} from './Controls/Index';
import {debounce} from '../utilities/helpers';

const Controls: any = {
    Boolean,
    Number,
    Stream
};

class DeviceComponent extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {};
    }

    async componentDidMount() {
        try {
            const control: any = await CustomDeviceService.getConfig(
                this.props.hub,
                this.props.component.deviceId,
                this.props.component.paramName
            );

            this.setState({control, [control.name]: control.value});
        } catch (e) {
            console.log("Failed to get config data", e);
        }
    }

    async handleControlChanged(value: any) {
        try {
            const result = await CustomDeviceService.updateControl(
                this.props.hub,
                this.props.component.deviceId,
                this.props.component.paramName,
                value
            );

            this.setState({[this.props.component.paramName]: result.value});
        } catch (e) {
            console.log("Failed to update control", e);
        }
    }

    renderControl() {
        if (this.state.control) {
            const control = this.state.control;
            const CustomControl = Controls[control.type];
            return <CustomControl name={control.name}
                                  value={this.state[control.name]}
                                  restrictions={control.restrictions}
                                  handleControlChanged={debounce(this.handleControlChanged.bind(this), 1000)}/>;
        }

        return null;
    }

    render() {
        return (<div style={gridItemStyle}>
            {this.props.component.name}
            {this.renderControl()}
        </div>)
    }
}

const gridItemStyle = {
    'backgroundColor': 'rgba(255, 255, 255, 0.8)',
    'border': '0px solid rgba(0, 0, 0, 0.8)',
    'justifySelf': 'center',
    'alignSelf': 'center'
};

export default DeviceComponent;

