import React from 'react';
import DeviceComponent from './DeviceComponent';

class Screen extends React.Component<any, any> {
    renderComponents() {
        return this.props.components.map((component: any, index: number) => {
            return <DeviceComponent key={index} hub={this.props.hub} component={component}/>;
        });
    }

    render() {
        return <div className="grid-container" style={gridContainerStyle}>
            {this.renderComponents()}
        </div>
    }
}

const gridContainerStyle = {
    'display': 'grid',
    'gridTemplateColumns': 'auto auto auto auto auto',
    'padding': '10px',
    'width': '100vw',
    'height': '100vh'
};

export default Screen;