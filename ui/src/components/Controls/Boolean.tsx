import React from 'react';
import BaseControl from "./BaseControl";

class Boolean extends BaseControl {
    onChange(event: any) {
        const value = event.target.checked;
        this.setState({value});
        this.props.handleControlChanged(value);
    }

    render() {
        return <input type="checkbox" checked={this.state.value} onChange={this.onChange.bind(this)}></input>
    }
}

export default Boolean;

