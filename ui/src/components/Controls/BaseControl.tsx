import React from 'react';

class BaseControl extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {value: this.props.value};
    }

    componentWillReceiveProps(nextProps: any) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        if (nextProps.value !== this.state.value) {
            this.setState({value: nextProps.value});
        }
    }
}

export default BaseControl;

