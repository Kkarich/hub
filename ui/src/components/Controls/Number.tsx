import React from 'react';
import BaseControl from "./BaseControl";

class Number extends BaseControl {
    onChange(event: any) {
        const value = parseInt(event.target.value);
        this.setState({value});
        this.props.handleControlChanged(value);
    }

    render() {
        return <input type="range" min={this.props.restrictions.min} max={this.props.restrictions.max}
                      value={this.state.value} onChange={this.onChange.bind(this)}></input>
    }
}

export default Number;